<?php
/**
 * Created by PhpStorm.
 * User: prendki
 * Date: 05.01.19
 * Time: 13:25
 */

namespace App\Service;


use App\Entity\Photo;
use Symfony\Component\DependencyInjection\Container;

class UploadPhotos extends Container
{
    public function upload(Photo $photo)
    {
        $photo->setName(md5(uniqid()) . '.' . $photo->getFile()->guessExtension());
        $photo->getFile()->move($this->getParameter('upload_directory'), $photo->getName());
    }
    public function uploadPhotos($photos) {
        foreach($photos as $photo) {
            if($photo){
                if($photo->getFile() != null) {
                    $this->upload($photo);
                }
            }
        }
    }
}