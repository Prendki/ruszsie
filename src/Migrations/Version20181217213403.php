<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181217213403 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE photo (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) DEFAULT NULL, url VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE survey_fb');
        $this->addSql('DROP TABLE survey_post');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE survey_fb (id INT AUTO_INCREMENT NOT NULL, id_fb VARCHAR(255) NOT NULL COLLATE utf8_general_ci, username_fb VARCHAR(255) DEFAULT NULL COLLATE utf8_general_ci, instagram_url LONGTEXT DEFAULT NULL COLLATE utf8_general_ci, email_fb VARCHAR(255) DEFAULT NULL COLLATE utf8_general_ci, firstname VARCHAR(255) DEFAULT NULL COLLATE utf8_general_ci, lastname VARCHAR(255) DEFAULT NULL COLLATE utf8_general_ci, email VARCHAR(255) DEFAULT NULL COLLATE utf8_general_ci, instagram_score VARCHAR(255) DEFAULT NULL COLLATE utf8_general_ci, no_acc TINYINT(1) DEFAULT \'0\', q1 LONGTEXT DEFAULT NULL COLLATE utf8_general_ci, q2 LONGTEXT DEFAULT NULL COLLATE utf8_general_ci, q3 LONGTEXT DEFAULT NULL COLLATE utf8_general_ci, q4 LONGTEXT DEFAULT NULL COLLATE utf8_general_ci, q5 LONGTEXT DEFAULT NULL COLLATE utf8_general_ci, q7 LONGTEXT DEFAULT NULL COLLATE utf8_general_ci, q8 LONGTEXT DEFAULT NULL COLLATE utf8_general_ci, created_at DATETIME DEFAULT NULL, confirm_token VARCHAR(255) DEFAULT NULL COLLATE utf8_general_ci, confirmed TINYINT(1) DEFAULT \'0\', ad_terms_1 TINYINT(1) DEFAULT \'0\', ad_terms_2 TINYINT(1) DEFAULT \'0\', ad_terms_3 TINYINT(1) DEFAULT \'0\', ad_terms_4 TINYINT(1) DEFAULT \'0\', UNIQUE INDEX id_fb (id_fb), UNIQUE INDEX id_UNIQUE (id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE survey_post (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, firstname VARCHAR(255) DEFAULT NULL COLLATE utf8_general_ci, lastname LONGTEXT DEFAULT NULL COLLATE utf8_general_ci, address VARCHAR(255) DEFAULT NULL COLLATE utf8_general_ci, phone VARCHAR(255) DEFAULT NULL COLLATE utf8_general_ci, city VARCHAR(255) DEFAULT NULL COLLATE utf8_general_ci, postal VARCHAR(255) DEFAULT NULL COLLATE utf8_general_ci, email VARCHAR(255) DEFAULT NULL COLLATE utf8_general_ci, is_over TINYINT(1) DEFAULT \'0\', ad_terms_1 TINYINT(1) DEFAULT \'0\', ad_terms_2 TINYINT(1) DEFAULT \'0\', UNIQUE INDEX id_UNIQUE (id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE photo');
    }
}
