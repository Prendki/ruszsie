<?php

namespace App\Controller\Site;

use App\Entity\ExerciseType;
use App\Entity\Page;
use App\Service\VueSerialize;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Symfony\Component\Serializer\Serializer;


class ExerciseTypeController extends AbstractController
{
    /**
     * @Route("/cwiczenia", name="site_exercise")
     */
    public function index(VueSerialize $vu)

    {
        $page = $this->getDoctrine()->getRepository(Page::class)->findOneBy(array("slug"=>"exercise"));
        $page = $vu->serialize($page);

        $exerciseTypes = $this->getDoctrine()
            ->getRepository(ExerciseType::class)
            ->findAll();

        $exerciseTypes = $vu->serialize($exerciseTypes);

        return $this->render('site/exercise-types/index.html.twig', [
            'page_name' => 'Typy Ćwiczeń',
            'exercise_types' => $exerciseTypes,
            'page_data' => $page
        ]);
    }
    /**
     * @Route("/cwiczenia/{name}", name="site_exercise_type")
     */
    public function show(ExerciseType $exercise_type)

    {
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceLimit(1);
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });

        $serializer = new Serializer([$normalizer], $encoders);

        $jsonContent = $serializer->serialize($exercise_type, 'json');

        return $this->render('site/exercise-types/show.html.twig', [
            'page_name' => $exercise_type->getName(),
            'exercise_types' => $jsonContent
        ]);
    }
}
