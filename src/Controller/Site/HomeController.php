<?php

namespace App\Controller\Site;

use App\Entity\Page;
use App\Service\VueSerialize;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends Controller
{
    /**
     * @Route("/endcap", name="home_endcap")
     */
    public function show_endcap(Request $request)
    {
        return $this->render('site/home/show.html.twig', [
            'page_name' => 'Strona Główna',
        ]);
    }

    /**
     * @Route("/", name="home")
     */
    public function show(Request $request, VueSerialize $vu)
    {
        $page = $this->getDoctrine()->getRepository(Page::class)->findOneBy(array("slug"=>"home"));
        $page = $vu->serialize($page);
        if ($request->cookies->has("visited")) {
            return $this->render('site/home/index.html.twig', [
                'page_name' => 'Strona Główna',
                'page_data' => $page
            ]);
        }

        $response = $this->render('site/home/show.html.twig', [
            'page_name' => 'Strona Główna',

        ]);

        $response->headers->setCookie(new Cookie('visited', 'true', time() + 3600 * 24 * 7, '/', null, false, false));

        return $response;
    }
}
