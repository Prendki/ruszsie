<?php

namespace App\Controller\Site;

use App\Entity\Exercise;
use App\Entity\ExerciseType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ExerciseController extends Controller
{
    /**
     * @Route("/cwiczenia/{exercise_type}/{exercise_name}", name="exercise")
     * @ParamConverter("exerciseType", options={"mapping": {"exercise_type" : "name"}})
     * @ParamConverter("exercise", options={"mapping": {"exercise_name"   : "name"}})
     */
    public function show(ExerciseType $exerciseType,Exercise $exercise)
    {
        $step = $exercise->getStep();
        $nextExercise = $this->getDoctrine()->getRepository(Exercise::class)->findOneBy(array('step' => $step+1,'exerciseType' => $exercise->getExerciseType()));
        $previousExercise = $this->getDoctrine()->getRepository(Exercise::class)->findOneBy(array('step' => $step-1,'exerciseType' => $exercise->getExerciseType()));
        $data = array("active" => $exercise, "next" => $nextExercise, "previous" => $previousExercise);

        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceLimit(1);
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });

        $serializer = new Serializer([$normalizer], $encoders);

        $jsonContent = $serializer->serialize($data, 'json');

        return $this->render('site/exercise/show.html.twig', [
            'page_name' => $exercise->getName(),
            'exercise' => $jsonContent
        ]);
    }
}
