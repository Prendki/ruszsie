<?php

namespace App\Controller\Admin;

use App\Entity\Exercise;
use App\Form\ExerciseType;
use App\Service\UploadPhotos;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ExerciseController extends Controller
{
    /**
     * @Route("/admin/exercise", name="admin_exercise")
     */
    public function index()
    {
        $repository = $this->getDoctrine()->getRepository(Exercise::class);

        $exercises = $repository->findBy(array(),array('name'=>'ASC','step'=> 'ASC'));
        return $this->render('admin/exercise/index.html.twig', [
            'page_name' => 'Lista ćwiczeń',
            'exercises' => $exercises
        ]);
    }
    /**
     * @Route("/admin/exercise/create", name="admin_exercise_create")
     */
    public function create(Request $request, UploadPhotos $up)
    {
        $exercise = new Exercise();
        $form = $this->createForm(ExerciseType::class,$exercise);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
             $up->uploadPhotos($exercise->getPhotos());
             $exercise->clearPhotos();
             $entityManager = $this->getDoctrine()->getManager();
             $entityManager->persist($exercise);
             $entityManager->flush();
            return $this->redirectToRoute('admin_exercise');
        }
        return $this->render('admin/exercise/create.html.twig', [
            'page_name' => 'Nowe ćwiczenie',
            'form' => $form->createView()
        ]);
    }
    /**
     * @Route("/admin/exercise/remove/{id}", name="admin_exercise_delete")
     */
    public function delete(Exercise $exercise)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($exercise);
        $entityManager->flush();
        return $this->redirectToRoute('admin_exercise');
    }
    /**
     * @Route("/admin/exercise/update/{id}", name="admin_exercise_update")
     */
    public function update(Exercise $exercise, Request $request,UploadPhotos $up)
    {
        $form = $this->createForm(ExerciseType::class,$exercise);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $up->uploadPhotos($exercise->getPhotos());
            $exercise->clearPhotos();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($exercise);
            $entityManager->flush();
            return $this->redirectToRoute('admin_exercise');
        }
        return $this->render('admin/exercise/edit.html.twig', [
            'page_name' => 'Edytuj ćwiczenie ćwiczenie',
            'form' => $form->createView()
        ]);
    }
}

