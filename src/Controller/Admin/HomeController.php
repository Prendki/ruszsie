<?php

namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("ROLE_ADMIN")
 * @Route("/admin")
 */
class HomeController extends AbstractController
{
    /**
     * @Route("", name="admin_board")
     */
    public function index()
    {
        return $this->render('admin/home/index.html.twig', [
            'page_name' => 'Admin Board',
        ]);
    }
}