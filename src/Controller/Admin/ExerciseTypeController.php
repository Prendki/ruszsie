<?php

namespace App\Controller\Admin;

use App\Entity\Exercise;
use App\Entity\ExerciseType;
use App\Form\ExerciseTypeType;
use App\Service\UploadPhotos;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ExerciseTypeController extends AbstractController
{
    /**
     * @Route("/admin/exercise_type", name="admin_exercise_type")
     */
    public function index()
    {
        $repository = $this->getDoctrine()->getRepository(ExerciseType::class);

        $exercises = $repository->findBy(array(),array('id'=>'ASC'));
        return $this->render('admin/exercise_type/index.html.twig', [
            'page_name' => 'Lista typów ćwiczeń',
            'exercises' => $exercises
        ]);
    }
    /**
     * @Route("/admin/exercise_type/create", name="admin_exercise_type_create")
     */
    public function create(Request $request, UploadPhotos $up)
    {
        $exercise = new ExerciseType();
        $form = $this->createForm(ExerciseTypeType::class,$exercise);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $up->uploadPhotos(array($exercise->getPhoto()));
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($exercise);
            $entityManager->flush();
            return $this->redirectToRoute('admin_exercise_type');
        }
        return $this->render('admin/exercise_type/create.html.twig', [
            'page_name' => 'Nowy typ ćwiczenia',
            'form' => $form->createView()
        ]);
    }
    /**
     * @Route("/admin/exercise_type/update/{id}", name="admin_exercise_type_update")
     */
    public function update(ExerciseType $exerciseType, Request $request, UploadPhotos $up)
    {
        $form = $this->createForm(ExerciseTypeType::class,$exerciseType);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $up->uploadPhotos(array($exerciseType->getPhoto()));
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($exerciseType);
            $entityManager->flush();
            return $this->redirectToRoute("admin_exercise_type");
        }
        return $this->render('admin/exercise_type/create.html.twig', [
            'page_name' => 'Edycja typu ćwiczenia',
            'form' => $form->createView()
        ]);
    }
    /**
     * @Route("/admin/exercise_type/remove/{id}", name="admin_exercise_type_delete")
     */
    public function delete(ExerciseType $exerciseType)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($exerciseType);
        $entityManager->flush();
        return $this->redirectToRoute('admin_exercise_type');
    }
}
