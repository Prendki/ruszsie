<?php

namespace App\Controller\Admin;

use App\Entity\Page;
use App\Form\PageType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PageController extends AbstractController
{
    /**
     * @Route("/admin/page", name="admin_pages")
     */
    public function index()
    {
        $repository = $this->getDoctrine()->getRepository(Page::class);

        $pages = $repository->findBy(array(),array('id'=>'ASC'));
        return $this->render('admin/page/index.html.twig', [
            'page_name' => 'Lista typów ćwiczeń',
            'pages' => $pages
        ]);
    }
    /**
     * @Route("/admin/page/create", name="admin_page_create")
     */
    public function create(Request $request)
    {
        $page = new Page();
        $form = $this->createForm(PageType::class,$page);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($page);
            $entityManager->flush();
            return $this->redirectToRoute('admin_pages');
        }
        return $this->render('admin/page/create.html.twig', [
            'page_name' => 'Nowa strona',
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/page/update/{id}", name="admin_page_update")
     */
    public function update(Page $page, Request $request)
    {
        $form = $this->createForm(PageType::class,$page);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($page);
            $entityManager->flush();
            return $this->redirectToRoute('admin_pages');
        }
        return $this->render('admin/page/create.html.twig', [
            'page_name' => 'Nowa strona',
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/page/delete/{id}", name="admin_page_delete")
     */
    public function delete(Page $page)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($page);
        $entityManager->flush();
        return $this->redirectToRoute('admin_pages');
    }
}
