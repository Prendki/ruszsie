<?php

namespace App\Command;

use App\Entity\Diet;
use Fidry\AliceDataFixtures\Bridge\Doctrine\Persister\ObjectManagerPersister;
use function MongoDB\BSON\toJSON;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class LoadFixtures extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('load:fixtures')

            // the short description shown while running "php bin/console list"
            ->setDescription('Ładowanie fixtures')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('Ta komenda pozwala na zaladowanie poczatkowych tresci...');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Ładowanie domyślnych danych',
            '============',
            '',
        ]);
        $loader = new \Nelmio\Alice\Loader\NativeLoader();
        $objectSet = $loader->loadFile(__DIR__."/../../config/fixtures.yaml");
        $entityManager = $this->getContainer()->get('doctrine')->getManager();
        foreach ($objectSet->getObjects() as $object) {
            $entityManager->persist($object);
        }
        $entityManager->flush();
        $output->writeln([
            'Ładowanie zakończone pomyślnie',
            '============',
            '',
        ]);
    }
}