<?php

namespace App\Command;


use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Console\Question\Question;


class CreateUser extends Command
{
    private $passwordEncoder;
    private $entityManager;

    public function __construct(EntityManagerInterface $em, UserPasswordEncoderInterface $encoder)
    {
        parent::__construct();
        $this->entityManager = $em;
        $this->passwordEncoder = $encoder;
    }
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('create:user')

            // the short description shown while running "php bin/console list"
            ->setDescription('Dodawanie użytkownika');

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $helper = $this->getHelper('question');
        $output->writeln([
            'Dodawanie użytkownika',
        ]);
        $question = new Question('Podaj email:','test@example.com');
        $email = $helper->ask($input, $output, $question);
        $question= new Question('Podaj nazwę użytkownika:','test');
        $username = $helper->ask($input, $output, $question);
        $question = new Question('Podaj hasło:','test');
        $password = $helper->ask($input, $output, $question);

        $user = new User();

        $user->setPassword($this->passwordEncoder->encodePassword($user,$password));
        $user->setEmail($email);
        $user->setUsername($username);
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $output->writeln([
            'Konto utworzone ? XD'
        ]);


    }
}