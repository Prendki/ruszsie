<?php

namespace App\Repository;

use App\Entity\DietImages;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method DietImages|null find($id, $lockMode = null, $lockVersion = null)
 * @method DietImages|null findOneBy(array $criteria, array $orderBy = null)
 * @method DietImages[]    findAll()
 * @method DietImages[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DietImagesRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, DietImages::class);
    }

//    /**
//     * @return DietImages[] Returns an array of DietImages objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DietImages
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
