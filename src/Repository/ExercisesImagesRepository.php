<?php

namespace App\Repository;

use App\Entity\ExercisesImages;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ExercisesImages|null find($id, $lockMode = null, $lockVersion = null)
 * @method ExercisesImages|null findOneBy(array $criteria, array $orderBy = null)
 * @method ExercisesImages[]    findAll()
 * @method ExercisesImages[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExercisesImagesRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ExercisesImages::class);
    }

//    /**
//     * @return ExercisesImages[] Returns an array of ExercisesImages objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ExercisesImages
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
