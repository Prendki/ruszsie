<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\ExercisesRepository")
 * @ORM\Table(name="exercises")
 */
class Exercise
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $text;

    /**
     * @ORM\Column(type="integer")
     */
    private $step;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $threshold;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ExerciseType", inversedBy="exercises")
     */
    private $exerciseType;

    /**
     * @var Photo[]
     * @ORM\ManyToMany(targetEntity="App\Entity\Photo",cascade={"persist"})
     * @ORM\JoinTable(
     *     name="exercise_photos",
     *     joinColumns={
     *      @ORM\JoinColumn(name="exercise_id", referencedColumnName="id")
     *     },
     *     inverseJoinColumns={
     *      @ORM\JoinColumn(name="photo_id", referencedColumnName="id")
     *     }
     * )
     */
    private $photos;

    /**
     * @return Photo[]
     */
    public function getPhotos()
    {
        return $this->photos ? $this->photos : [];
    }

    /**
     * @param Photo[] $photos
     */
    public function setPhotos(array $photos): void
    {

        $this->photos = $photos;
    }


    /**
     * @return mixed
     */
    public
    function getStep()
    {
        return $this->step;
    }

    /**
     * @param mixed $step
     */
    public
    function setStep($step): void
    {
        $this->step = $step;
    }

    public
    function getId(): ?int
    {
        return $this->id;
    }

    public
    function getName(): ?string
    {
        return $this->name;
    }

    public
    function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public
    function getText(): ?string
    {
        return $this->text;
    }

    public
    function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public
    function getThreshold(): ?string
    {
        return $this->threshold;
    }

    public
    function setThreshold(?string $threshold): self
    {
        $this->threshold = $threshold;

        return $this;
    }

    public
    function setExerciseType($exerciseType): void
    {
        $this->exerciseType = $exerciseType;
    }

    public
    function getExerciseType()
    {
        return $this->exerciseType;
    }

    public
    function __toString()
    {

        return $this->name;
    }

    public
    function addPhoto($photo)
    {
        $this->photos[] = $photo;
    }

    public function clearPhotos()
    {
        $array = $this->getPhotos();
        if(!is_array($array)){
            $array = $array->toArray();
        }
        $array = array_diff($array,[null]);


        $array = array_filter($array, function ($element) {
            return ($element->getName() !== null || $element->getFile() !== null);
        });
        $this->setPhotos($array);
    }
}