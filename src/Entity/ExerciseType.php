<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\ExerciseTypesRepository")
 * @ORM\Table(name="exercise_types")
 */
class ExerciseType
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Exercise", mappedBy="exerciseType")
     * @ORM\OrderBy({"step" = "ASC"})
     */
    private $exercises;

    /**
     * @ORM\Column(type="text",nullable=true)
     */

    private $description;

    /**
     * @ORM\Column(type="text",nullable=true)
     */

    private $parts_description;

    /**
     * @var Photo
     * @ORM\OneToOne(targetEntity="App\Entity\Photo",cascade={"persist"},)
     * @ORM\JoinTable(
     *     name="exercise_photos",
     *     joinColumns={
     *      @ORM\JoinColumn(name="exercise_id", referencedColumnName="id")
     *     },
     *     inverseJoinColumns={
     *      @ORM\JoinColumn(name="photo_id", referencedColumnName="id")
     *     }
     * )
     */
    private $photo;

    /**
     * @return Photo
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * @param Photo $photo
     */
    public function setPhoto(Photo $photo): void
    {
        $this->photo = $photo;
    }

    /**
     * @return mixed
     */
    public function getPartsDescription()
    {
        return $this->parts_description;
    }

    /**
     * @param mixed $parts_description
     */
    public function setPartsDescription($parts_description): void
    {
        $this->parts_description = $parts_description;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }


    /**
     * ExerciseType constructor.
     */
    public function __construct()
    {
        $this->exercises = new ArrayCollection();
    }

    /**
     * @return Collection|Exercise[]
     */
    public function getExercises(): Collection
    {
        return $this->exercises;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
    public function __toString(){
        return $this->name;
    }
}
