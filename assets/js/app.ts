import Vue from 'vue';
import Vuex from 'vuex'
import HomeModal from './components/pages/Home/home-modal.vue';
import Home from './components/pages/Home/home.vue';
import ExerciseTypes from './components/pages/ExerciseTypes/exercise-types.vue';
import ExerciseType from './components/pages/ExerciseType/exercise-type.vue';
import Exercise from './components/pages/Exercise/exercise.vue';
import AppLogo from './components/partials/app-logo.vue';
import store from './store/store';


Vue.use(Vuex);

let src = document.getElementById("app");
new Vue({
    el: src,
    store,
    components: {
        Home,
        HomeModal,
        AppLogo,
        ExerciseTypes,
        ExerciseType,
        Exercise
    }
});

