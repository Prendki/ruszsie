// initial state
const state = {
    page_name : "",
    menu_active : false
};

// getters
const getters = {
    getPageName(state) {
        return state.page_name;
    },
    getMenuActive(state) {
        return state.menu_active;
    }
};

// actions
const actions = {

};

// mutations
const mutations = {
    setPageName(state,value) {
        state.page_name = value;
    },
    setMenuActive(state,value) {
        state.menu_active = value;
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}