// initial state
const state = {
    page_data_object : {},
    page_data_array : [],
    page_element_active : null
};

// getters
const getters = {
    getPageDataObject(state) {
        return state.page_data_object;
    },
    getPageDataArray(state) {
        return state.page_data_array;
    },
    getPageElementActive(state) {
        return state.page_element_active;
    }
};

// actions
const actions = {

};

// mutations
const mutations = {
    setPageDataObject(state,value) {
        state.page_data_object = value;
    },
    setPageDataArray(state,value) {
        state.page_data_array = value;
    },
    setPageElementActive(state,value) {
        state.page_element_active = value;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}