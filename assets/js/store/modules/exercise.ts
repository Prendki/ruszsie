// initial state
const state = {
    exercise_types : [],
    exercise_active : null
};

// getters
const getters = {
    getExerciseTypes(state) {
        return state.exercise_types;
    },
    getExerciseActive(state) {
        return state.exercise_active;
    }
};

// actions
const actions = {

};

// mutations
const mutations = {
    setExerciseTypes(state,value) {
        state.exercise_types = value;
    },
    setExerciseActive(state,value) {
        state.exercise_active = value;
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}