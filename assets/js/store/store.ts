import Vue from 'vue';
import Vuex from 'vuex';
import base from './modules/base';
import exercise from './modules/exercise';
import page from './modules/page';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        base,
        exercise,
        page
    },
})